# Python_Basic
# Wooify

Wooify is a script to prepare eCommerce product data for import into the
[Woo Import Export](https://codecanyon.net/item/woo-import-export/13694764)
WordPress plugin.

## Supported Sources

- [Ecwid](https://support.ecwid.com/hc/en-us/articles/207099979-Import-Export)
- [DPSounds](https://dpsounds.com) custom CSV format

## Usage
`wooify.py adapter input_file`

- `adapter`: an import adapter
- `input_file`: the source file you want to convert

Available import adapters: `ecwid`

Output will be written to:

- `woocommerce_products.csv`
- `woocommerce_categories.csv`

