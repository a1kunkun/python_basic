import os
from setuptools import setup

def read(fname):
    """Return contents of named file as a string."""
    return open(os.path.join(os.path.dirname(__file__), fname)).read()

setup(
    name='Wooify',
    license='MIT',
    author='Anton Sarukhanov',
    author_email='code@ant.sr',
    description='A script to prepare eCommerce product data'
        'for import into the "Woo Import Export" WordPress plugin.',
    long_description=read('README.md'),
    packages=['wooify', 'tests'],
    keywords='woocommerce wordpress products ecommerce',
    install_requires=[
        'awesome-slugify==1.6.5',
        'lxml==3.7.0',
        'phpserialize==1.3',
    ],
    entry_points='''
        [console_scripts]
        wooify=wooify.__main__:main
    ''',
)
